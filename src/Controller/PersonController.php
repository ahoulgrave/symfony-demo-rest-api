<?php
namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Response\FormErrorResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PersonController extends AbstractController
{
    public function getPerson(Request $request) {
        $person = new Person('Alberto', 'Olmedo');
        $person2 = new Person('Gonzalo', 'Celina');

        return new JsonResponse([
            $person,
            $person2,
        ]);
    }

    public function createPerson(Request $request)
    {
        $data = $request->get('jsonContent');

        $form = $this->createForm(PersonType::class);
        $form->submit($data);

        if (!$form->isValid()) {
            return new FormErrorResponse($form);
        }

        // persist in storage
        // $this->personRepository->persist($form->getData());

        return new JsonResponse($data);
    }
}