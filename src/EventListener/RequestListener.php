<?php
namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestListener
{
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $key = $event->getRequest()->headers->get('X-Auth-Brown');

        if ($key !== 'brown123') {
            $event->setResponse(new JsonResponse(['message' => 'invalid key'], Response::HTTP_FORBIDDEN));
        }

        if ($event->getRequest()->headers->get('Content-Type') !== 'application/json') {
            return;
        }

        $data = json_decode($event->getRequest()->getContent(), true);

        if (json_last_error()) {
            $event->setResponse(new JsonResponse(['error' => 'Invalid json'], Response::HTTP_BAD_REQUEST));
            return;
        }

        $event->getRequest()->attributes->set('jsonContent', $data);
    }
}