<?php
namespace App\Response;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FormErrorResponse extends JsonResponse
{
    public function __construct(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            $errors[] = [
                'message' => $error->getMessage(),
                'field' => $error->getOrigin() ? $error->getOrigin()->getName() : 'none',
            ];
        }

        parent::__construct([
            'message' => 'Form error',
            'errors' => $errors,
        ], Response::HTTP_BAD_REQUEST);
    }
}